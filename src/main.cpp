/* ***************************************************************************
* File:    main.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT Public License
*
* ******************************************************************************/


/* ---
# LumosStick "Falling Sticks" Animation

### Fabrication

The electronics of the LumosStick:
  - ESP32-S2 (Lolin Mini S2)
  - 280 WS2812C-2020 LEDs (7 strips of 40 LEDs)
  - magnetic speaker
  - 2 edge mounted tact switches


**NOTE:** For the purposes of the comments below,
orientation of the LumosStick is assumed to be vertical.
This means the x-axis is 7 and the y-axis is 40.
The edge buttons are assumed to be at the top.

--------------------------------------------------------------------------
--- */

#include <functional>

#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

/* ---
--------------------------------------------------------------------------

 #### Animation:

The bounce animation has been adapted from the work of KenKen and the Bounce-LED project.
https://github.com/KenKenMkIISR/Bouncing-LED.git

The code has been converted from the Adafruit Neopixel library to FastLED. The code was then expanded to
multiple strips with each strip on its own control pin. The cascade algorithm was created to eventually fill
all strips before flushing the display clear and beginning again. A simple _slinky_ transition was added when
the block moves from one strip to the next.

**Disclaimer:**

This project was done as a passtime and thus was not really the tidiest or most well commented solution.
The code does have a lot of useful notes but it's likely there are assumptions which have not been well explained.

--- */

// #define REVERSE_DIRECTION	// rotate the LumosStick 180 degrees so the edge buttons are at the bottom

#define LEDS_DEFAULT_BRIGHTNESS 63	// reduce total brightness to about 25% to conserve power
#define LEDS_LOW_BRIGHTNESS 15		// lower threshold before we override the actual LED color to render

// These values are specific to the LumosStick; you could change them to lower values but not higher ... unless you run this code on different hardware

#define SEGMENT_LEN	  40			// each segment has 40 LEDs
#define SEGMENT_COUNT 7				// there are 7 segments, each has its own control pin

// these are the GPIO pins on the LumosStick connected to the LEDs

#define SEGMENT_PIN1 33				// the GPIO pins of the controller module
#define SEGMENT_PIN2 34
#define SEGMENT_PIN3 35
#define SEGMENT_PIN4 36
#define SEGMENT_PIN5 37
#define SEGMENT_PIN6 38
#define SEGMENT_PIN7 39

// we create a set of FastLED LEDs for each GPIO pin

static CRGB g_leds1[SEGMENT_LEN];
static CRGB g_leds2[SEGMENT_LEN];
static CRGB g_leds3[SEGMENT_LEN];
static CRGB g_leds4[SEGMENT_LEN];
static CRGB g_leds5[SEGMENT_LEN];
static CRGB g_leds6[SEGMENT_LEN];
static CRGB g_leds7[SEGMENT_LEN];

// We combine the individual strings of LEDs, into a matrix

#ifdef REVERSE_DIRECTION
static CRGB *g_led_strips[SEGMENT_COUNT] = {g_leds1, g_leds2, g_leds3, g_leds4, g_leds5, g_leds6, g_leds7}; // convenience array
#else
static CRGB *g_led_strips[SEGMENT_COUNT] = {g_leds7, g_leds6, g_leds5, g_leds4, g_leds3, g_leds2, g_leds1}; // convenience array
#endif


static bool _leds_inited = false;
static bool g_leds_dirty = false;


// NOTE: we use fixed point integers and calculations are performed at 256* (aka we shift by 8 to get real value)
#define PHYSICAL_VAL(val)  ((val) >> 8)
#define PRECISION_VAL(val) ((val) << 8)


// bounce coefficient at the lowest point is (R1 / R2)

#define GRAVITY 	10 // simple acceleration

#define R1		3  // bounce coefficient 1
#define R2		5  // bounce coefficient 2
#define REFMINV 60 // minimum rebound speed


// these keep track fo the active falling stick

#define STICK_LEN	4  // length of led block

uint8_t g_stick_state = 0;
uint8_t g_stick_color = 1;
uint8_t g_stick_strip_first = 0;
uint8_t g_stick_strip = 0;
int32_t g_stick_speed = 0;

int32_t g_stick_speeds[SEGMENT_COUNT];
int32_t g_stick_positions[SEGMENT_COUNT];
int32_t g_stick_physical_move[SEGMENT_COUNT]; // used by flush
int32_t g_column_tops[SEGMENT_COUNT];


// -- ---------------- ---------------------------------------------------------------------------------------------------------------------
// -- FUNCTIONS ----------------------------------------------------------------------------------------------------------------------------
// -- ---------------- ---------------------------------------------------------------------------------------------------------------------


void ledsClear() {
	// Serial.print("Clear\n");
	for (int i = 0; i < SEGMENT_COUNT; i++) {
		CRGB *strip = g_led_strips[i];
		for (int j = 0; j < SEGMENT_LEN; j++) {
			strip[j] = CRGB::Black;
		}
	}
}


void ledsRefresh() {
	if (!_leds_inited)
		return;

	if (g_leds_dirty) {
		FastLED.show();
		//Serial.print("Show\n");
	} else {
		//Serial.print("No Show\n");
	}

	g_leds_dirty = false;
	// Serial.println("");
}


void ledsBrightness(uint8_t i) {
	FastLED.setBrightness(i);
	//Serial.printf("LEDs Max Brightness = % d\n", i);
	g_leds_dirty = true;
}


bool ledSetColor(uint16_t column, uint16_t led, uint8_t r, uint8_t g, uint8_t b) {
	CRGB *leds = NULL;

	if (column < SEGMENT_COUNT) {
		leds = g_led_strips[column];

#ifndef REVERSE_DIRECTION
		led = (SEGMENT_LEN - 1) - led;
#endif
		if (led < SEGMENT_LEN) {
			leds[led] = CRGB(r, g, b); // need to figure out my mapping error
			// Serial.printf("[%d:%04d] (%3d, %3d, %3d)    \n", strip,led, r, g, b);
		} else {
			// Serial.printf("[%04d] (inv)\n", led);
		}
		g_leds_dirty = true;
	} else {
		//Serial.printf("invalid strip index %d\n", strip);
	}
	return true;
}

void ledShiftColumn(uint8_t column, int32_t start, int16_t amount) {
	//Serial.printf("shift position = %5d, by amount = %5d\n", start, amount);

	if (column >= SEGMENT_COUNT)
		return;
	if (start < 0)
		return;
	if (amount <= 0)
		return;

	CRGB *s = g_led_strips[column];

	if (start >= SEGMENT_LEN)
		start = SEGMENT_LEN - 1;

	if (amount < start) {
		// shift leds down
		for (int i = 0; i <= (start - amount); i++) {
#ifdef REVERSE_DIRECTION
			s[i] = s[i + amount];	// normal direction
#else
			s[(SEGMENT_LEN - 1) - i] = s[(SEGMENT_LEN - 1) - (i + amount)];	// normal direction
#endif
		}
	} else {
		amount = start;
	}
	for (int i = 0; i <= amount; i++) {
		if (((start - i) >= 0) && ((start - i) < SEGMENT_LEN))
			s[(SEGMENT_LEN - 1) - (start - i)] = CRGB::Black;
	}

	g_leds_dirty = true;
}


void ledSetColor(uint8_t column, int32_t top_led, uint8_t c) {
	// Serial.printf("block position = %5d, block color = %5d\n", top_led, c);

	// y = the beginning position of the block
	// c = color (0 = black, 1 = red, 2= green, 3 = blue

	uint8_t r = 0, g = 0, b = 0;

	r = 0x1F * (c & 0x03);
	g = 0x1F * ((c >> 2) & 0x03);
	b = 0x1F * ((c >> 4) & 0x03);

	if (top_led >= SEGMENT_LEN)
		top_led = SEGMENT_LEN;

	ledSetColor(column, top_led, r, g, b);

	for (uint8_t i = 0; i < STICK_LEN; i++) {
		if (top_led == 0)
			break;
		top_led--;
	}
}


void ledSetStick(uint8_t column, int32_t top_led, uint8_t c) {
	// Serial.printf("block position = %5d, block color = %5d\n", top_led, c);
	for (uint8_t i = 0; i < STICK_LEN; i++) {
		ledSetColor(column, top_led, c);
		if (top_led == 0)
			break;
		top_led--;
	}
}


void matrixUpdate() {

	/*
		matrix update:
		This is a simple state machine where there are five states (0..4)
		init		- initialize all of the global data
		dropping	- this is the main task of falling sticks, bouncing, and stacking
		pre-flush	- the matrix is full
		flush		- animation of all the columns of sticks falling down of the bottom
		reset		- the entire sequence has completed; the next state is back to 'init'
	*/

	switch (g_stick_state) {

		case 0: { // init
			ledsClear();
			g_stick_color = random(1, 16);
			g_stick_speed = random(0, GRAVITY);
			g_stick_strip = 0;
			g_stick_strip_first = 0;

			for (int i = 0; i < SEGMENT_COUNT; i++) {
				g_column_tops[i] = 0;							   // initial bounce point
				g_stick_positions[i] = PRECISION_VAL(SEGMENT_LEN - 1); // initial drop point is the very top
				g_stick_physical_move[i] = 0;						   // amount of physical movement is zero
			}
			g_stick_state++;
		} break;

		case 1: { // dropping stick
			ledSetStick(g_stick_strip, PHYSICAL_VAL(g_stick_positions[g_stick_strip]), g_stick_color); 				// set pixels for block's position
			ledsRefresh();
			ledSetStick(g_stick_strip, PHYSICAL_VAL(g_stick_positions[g_stick_strip]), 0); 							// clear block's position (but do not show it yet) because we are able to calculate a new position

			g_stick_positions[g_stick_strip] = g_stick_positions[g_stick_strip] - g_stick_speed; 					// update the block position

			if (g_stick_positions[g_stick_strip] <= (g_column_tops[g_stick_strip] + PRECISION_VAL(STICK_LEN-1))) {	// check for collision with bottom (or top static block)
				g_stick_positions[g_stick_strip] = g_column_tops[g_stick_strip] + PRECISION_VAL(STICK_LEN-1);	  	// make sure we are at the collision position (not beyond it)
				g_stick_speed = -((g_stick_speed)*R1 / R2);														  	// calculate bounce speed (value is negative when going up)

				if (g_stick_speed > (-(REFMINV))) {																		 	// stop when bounce speed is at or below our minimum speed
					ledSetStick(g_stick_strip, PHYSICAL_VAL(g_column_tops[g_stick_strip]) + (STICK_LEN-1), g_stick_color);	// fix the block position to just on top of whatever is the lowest previous bottom
					g_column_tops[g_stick_strip] = g_column_tops[g_stick_strip] + PRECISION_VAL(STICK_LEN);					// set new lowest top

					if (PHYSICAL_VAL(g_column_tops[g_stick_strip_first]) >= SEGMENT_LEN) { 							// if the first segment is full
						g_stick_strip_first++;													 					// start from the next
					}
					g_stick_strip = g_stick_strip_first;
					if (g_stick_strip < SEGMENT_COUNT)
						g_stick_positions[g_stick_strip] = PRECISION_VAL(SEGMENT_LEN); // top of strip
					g_stick_speed = random(0, GRAVITY);
#if 1
					g_stick_color = random(1, 0x40);
#else
					g_stick_color++; // next color
					if (g_stick_color > 7)
						g_stick_color = 1;
#endif
				}
			}
			else {
				/*
					if the stick has reached apogee, then we want to attempt to flow over to the next segment
					apogee is determined when acceleration transitions from negative to positive
					we can only flow over to the next segment if that segment's bounce point (aka top) is below our current position
				*/
				g_stick_speed = g_stick_speed + GRAVITY;																		  // update the falling speed
				if ((g_stick_speed >= 0) && ((g_stick_speed - GRAVITY) < 0)) {													  // apogee
					if (g_stick_strip < (SEGMENT_COUNT - 1)) {																	  // there is an adjacent strip
						if (g_stick_positions[g_stick_strip] > (g_column_tops[g_stick_strip + 1] + PRECISION_VAL(STICK_LEN))) { // the adjacent strip top is below our current position

							// attempt a slinky type animation transition from one strip to the next
							ledSetStick(g_stick_strip, PHYSICAL_VAL(g_stick_positions[g_stick_strip]), g_stick_color); // set pixels for block's position
							for (int i = (STICK_LEN - 1); i >= 0; i--) {
								ledSetColor(g_stick_strip, PHYSICAL_VAL(g_stick_positions[g_stick_strip]) - i, 0);
								ledSetColor(g_stick_strip + 1, PHYSICAL_VAL(g_stick_positions[g_stick_strip]) - ((STICK_LEN - 1) - i), g_stick_color);
								ledsRefresh();
								if (i > 0) // don't delay on the last iteration
									delay(50);
							}

							g_stick_positions[g_stick_strip + 1] = g_stick_positions[g_stick_strip]; // shift current position over
							g_stick_positions[g_stick_strip] = PRECISION_VAL(SEGMENT_LEN);			 // reset current position
							g_stick_strip++;
						}
					}
				}
			}

			//ledsRefresh();
			if (g_stick_strip_first >= SEGMENT_COUNT) {
				// setup for next state
				g_stick_state++;
			}
		} break;

		case 2: { // pre-flush
			g_stick_speed = 0;
			g_stick_strip = 0;
			g_stick_strip_first = 0;
			for (int i = 0; i < SEGMENT_COUNT; i++) {
				g_stick_speeds[i] = random(0, GRAVITY/2);
				g_column_tops[i] = 0;							   // initial bounce point
				g_stick_positions[i] = PRECISION_VAL(SEGMENT_LEN - 1); // initial drop point is the very top
				g_stick_physical_move[i] = 0;						   // amount of physical movement is zero
			}
			g_stick_state++;
		} break;

		case 3: { // flushing matrix
			for (int i = 0; i <= g_stick_strip_first; i++) 			{
#ifdef REVERSE_DIRECTION
				ledShiftColumn(i, (SEGMENT_LEN - 1) - (PHYSICAL_VAL(g_stick_positions[i]) + g_stick_physical_move[i]), g_stick_physical_move[i]);
#else
				ledShiftColumn(i, PHYSICAL_VAL(g_stick_positions[i]) + g_stick_physical_move[i], g_stick_physical_move[i]);
#endif
				g_stick_speeds[i] = g_stick_speeds[i] + (GRAVITY / 4); // update speed (with simulated friction)

				g_stick_physical_move[i] = PHYSICAL_VAL(g_stick_positions[i]);
				g_stick_positions[i] = g_stick_positions[i] - g_stick_speeds[i]; // top position (changes rate as acceleration changes)
				g_stick_physical_move[i] -= PHYSICAL_VAL(g_stick_positions[i]);
			}
			ledsRefresh();
			//delay(10);
			if (g_stick_speeds[g_stick_strip_first] > (GRAVITY * 2)) // delay when we start to flush each strip
				g_stick_strip_first++;

			if (g_stick_strip_first >= SEGMENT_COUNT) // only increment up to the number of segments we have available
				g_stick_strip_first = SEGMENT_COUNT - 1;

			if (g_stick_positions[SEGMENT_COUNT - 1] < 0) { // when the last strip is empty, move to the next state
				g_stick_state++;
			}
		} break;

		case 4: { // reset system
			g_stick_state = 0;
			ledsClear();
			ledsRefresh();
			delay(500);
		} break;
	}
}


/* ---
#### ledsInit()
Performs all necessary initialization. Must be called once before using any of the other LEDS functions.
 - return: **bool** true on success and false on failure
--- */
bool ledsInit() {
	if (_leds_inited)
		return true;

	//Serial.print("Initializing LED strips\n");

	// can not do this in a loop since the FastLED declaration is performed at compile time
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN1>(g_leds1, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN2>(g_leds2, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN3>(g_leds3, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN4>(g_leds4, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN5>(g_leds5, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN6>(g_leds6, SEGMENT_LEN);
	FastLED.addLeds<NEOPIXEL, SEGMENT_PIN7>(g_leds7, SEGMENT_LEN);

	FastLED.setCorrection(0xFFBFEF); // tone down the green a bit
	ledsBrightness(LEDS_DEFAULT_BRIGHTNESS);

	delay(100); // needed to give FastLED time to get ready? not sure
	g_leds_dirty = true;
	_leds_inited = true;
	g_stick_state = 0;
	g_stick_positions[g_stick_strip] = 0;
	g_stick_color = 1;

	ledsClear();
	ledsRefresh();

	return _leds_inited;
}


/* ---
#### ledsLoop()
Perform any LED operations which need to be regularly - this includes the 7-segment and dot clock rendering
 - return: **none**
--- */

void ledsLoop() {
	if (!_leds_inited)
		return;

	matrixUpdate();
}



// -----------------------------------------------------------
// the standard Arduino entrypoints setup() and loop()
// -----------------------------------------------------------

//static int _main_heap;

void setup() {
	//Serial.begin(115200);
	//Serial.println("");
	//Serial.println("");

	randomSeed(millis());

	//Serial.printf("Starting with random value = %d\n", (int)random(256));
	delay(100);

	ledsInit(); // init them early so they can be used to indicate init progress
	ledsClear(); // indicate end of initialization

	//Serial.print("---- System Initialized ----\n");
	delay(50);

	//_main_heap = ESP.getFreeHeap();

}

void loop() {
	ledsLoop();
	//delay(1);
}
