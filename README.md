
Yeah, there really should be something here but there isn't.

What you really want to see is in `src/main.cpp`.
There you will find all of the interesting code to perform the
bouncy animation (based off of work by **KenKen** on **GitHub**) and
my cascading animation work.
